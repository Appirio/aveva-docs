## Release Calendar

!!!Note
	**To view detailed Release Calendar & book a release slot.** [**Navigate here**]( https://wipro365-my.sharepoint.com/:x:/g/personal/ak20110752_wipro_com/ERzXEltofQNFvEmjp5G6-PoBxkR-VSX1cJFjIwW7lpNEQg){:target="_blank"}

Release Calendar provides us with a way to view detailed information regarding upcoming releases.

Before planning the next release, it's necessary to take a look at the current Release Calendar.
![image alt text](images/RC.png)

## Book a Release slot on the Release Calendar

Follow these steps for booking a slot on the Release Calendar:

1. Review the existing Release Calendar and decide when do you want to move your changes & what type of release do you want to target: Major, minor, hypercare.
1. Once decided then send a mail to the RM team with upcoming release details such as:

    - SIT to UAT movement Date
    - UAT Testing Dates
    - UAT Freeze Date
    - Production deployment Date

1.  RM team will check the release calendar making sure this proposed release is not interfering with any other team's schedule/salesforce upgrades.
1.  RM team will send out an e-mail to all the teams to advertize the addition/change to the Release Calendar. If there are any changes needed, they will be done after discussing with all the teams. So everyone is on the same page.
1.  Release date will be successfully marked on the calendar by RM team.
