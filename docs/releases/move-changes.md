Each environment has a different process of moving changes & depending on the scenario, we either cherry-pick or perform whole branch merges.

!!!Warning
	Be doubly sure when moving changes from your feature branch to SIT, whenever in confusion, please go through the below documentation or contact RM team for clarity. Remember prevention is always better than cure.

If you have the privilege and clearance level to merge changes into a shared branch connected to an environment then be really careful and mindful of the process and be **doubly sure** about the source branch, target branch and the changes you're moving through.

!!!Tip
	If they are any Pre-Manual steps perform those steps in the target org before moving your changes.

## Moving changes to QA/SIT

When moving changes from the feature branch to SIT, be careful as to not overwrite other teams' changes. There might be some changes which are present on the SIT branch but aren't available in your Dev environment yet, so while committing your changes in your feature branch **make sure to not override any of the existing changes.** If you see any contradictory/conflicting changes or a different version of the file in the repository always assume that the other team has made those changes and then verify it by checking the history of that file on GitLab.

1. Create your feature branch.

	!!! Tip
		Read [working on a feature](https://appirio.gitlab.io/aveva-docs/develop/feature-branch/#create-a-feature-branch) for a detailed overview of this approach.

1. Commit your changes to the feature branch.

1. Make sure they're validated successfully against QA/SIT.

1. Raise a merge request and assign it to your tech lead for merging into QA/SIT.

	!!!Warning
			Make sure to fill [Manual steps sheets](https://appirio.gitlab.io/aveva-docs/develop/forms/#manual-steps-sheets) as well if your story has any manual steps.

1. You will also need to perform these manual steps on QA/SIT yourself.

## Moving changes to UAT

1. To move any story/feature to UAT developers need to fill the [UAT-Dev Handoff form](https://appirio.gitlab.io/aveva-docs/develop/forms/#uat-dev-hand-off-form). The release managers will get a notification when this is filled.

1. They will open the smartsheets.

1. Open the GitLab merge request make sure it's successfully deployed.

1. Get the commit ID from the merge request.

	![image alt text](images/merge-request.png)

1. Create a `uatValidate/<dev_name>-<story_issue_incident_number>-<story_issue_incident_short_description>`
 branch from UAT branch.

1. Cherry pick the commit ID copied from the merge request in to the `uatValidate/<dev_name>-<story_issue_incident_number>-<story_issue_incident_short_description>` branch.

	!!! tip
		Read through [the cherry-pick deployments page](https://appirio.gitlab.io/aveva-docs/releases/cherry-pick-deployments/) to learn about cherry-picking.

1. This will trigger a validation to UAT. If it has some errors make sure why they are occurring, resolve them and then once the validation is successful they will create a merge request to the UAT environment from the `uatValidate/<dev_name>-<story_issue_incident_number>-<story_issue_incident_short_description>` branch.

1. This merge request will be merged in the UAT branch by the Release Manager deploying changes to the UAT environment and validating changes against the Production environment.

## Moving changes to Production

1. When UAT has been done, and the business has given a green signal to move changes to production environment all changes from the UAT must go to the production.

1. For this, the RM goes and creates a merge request with `UAT as the source branch` and `master as the target branch`.

1. Once the RM approves the merge request, the changes are again validated against the production environment.

1. To trigger the deployment to production now, the RM needs to create a tag. Go through the tip below to learn more about it.

!!! Tip
		To schedule deployment to production job [follow the steps mentioned here](https://dx.appirio.com/docs/best-practices-salesforce/legacy-salesforce-dev-workflow/#release-to-production){:target="_blank"}.


## Moving selective changes to Production

!!! Warning
		These steps should only be followed to move hotfixes to production when some other project is in UAT and some desperate hotfix needs to be delivered to the production environment which can't wait to move along the project in UAT to production.

1. Create a `prodValidate/<dev_name>-<story_issue_incident_number>-<story_issue_incident_short_description>`
 branch from master branch.

1. Cherry pick the commit to the `prodValidate/<dev_name>-<story_issue_incident_number>-<story_issue_incident_short_description>` branch.

1. This will trigger a validation to Production. If it has some errors make sure why they are occurring, resolve them and then once the validation is successful they will create a merge request to the master branch from the `prodValidate/<dev_name>-<story_issue_incident_number>-<story_issue_incident_short_description>` branch.

1. This merge request will be merged in the master branch by the Release Manager again triggering a validation to production and then the deployment tag can be created to schedule a manual production deployment job.
