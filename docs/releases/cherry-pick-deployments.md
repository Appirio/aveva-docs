Cherry-pick is a git command that selects specific commits for merging into a branch, by using this command we can selectively commit changes to our release branch for deployment.

Cherry-picking can be done via the command line as well but GitLab makes it way easy to cherry-pick commits to move within branches.

1. So when a Release Manager wants to move a story/feature to UAT. They open its SIT merge request (The link should be in the UAT Handoff form which dev filled). It would look something like this:

	![image alt text](images/merged-mr.png)

1. Before, clicking on the cherry-pick button, first go ahead and create a `uatValidate/<dev_name>-<story_issue_incident_number>-<story_issue_incident_short_description>` branch from **UAT**.

	![image alt text](images/uatvalidate-branch.png)

1. Now, go back to the closed SIT merge request and click on cherry-pick. It will open a dialog box asking you in which branch do you want to cherry-pick this commit. Select your newly created `uatValidate/<dev_name>-<story_issue_incident_number>-<story_issue_incident_short_description>` branch.

	![image alt text](images/choose-branch.png)


	!!! Note
			If you do not see your new branch here, refresh this page and try clicking cherry-pick again. You should see your branch in the dropdown now.


1. If your changes were simple enough, then your changes would be cherry-picked in your `uatValidate/<dev_name>-<story_issue_incident_number>-<story_issue_incident_short_description>` branch and a pipeline validating those changes to UAT would be triggered.

	![image alt text](images/new-cherry-pick-mr.png)

!!! Warning
	It is possible that you face merge conflicts while cherry-picking the changes, in that case you'll need to resolve the conflicts. Contact a senior release manager or try cherry-picking via the command line to proceed in that scenario.
