Follow these steps for booking a slot on the Release Calendar:

1.  Send a mail to the RM team(Akash + Abhishek) with Upcoming Release details such as:

    - SIT to UAT movement Date
    - UAT Testing Dates
    - UAT Freeze Date
    - Production deployment date

2.  RM team will check the release calendar making sure this proposed release is not interfering with any other team's schedule/salesforce upgrades.
3.  RM team will send out an e-mail to all the teams to advertize the addition/change to the Release Calendar. If there are any changes needed, they will be done after discussing with all the teams. So everyone is on the same page.
4.  Release date will be successfully marked on the calendar by RM team.
