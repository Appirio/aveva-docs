# Activities to conduct after a release

## Manual Steps
Always remember to perform the Post-Manual Steps to the target org after the deployment. Don't forget to update the smartsheet to show till which environment manual steps have been performed.

!!!Info 
	To access the manual step sheets [navigate here](https://appirio.gitlab.io/aveva-docs/develop/forms/#manual-steps-sheets).
	
## Reverse Sync Dev Sandboxes

Individual Dev Sandboxes get out of sync with the latest features that have been deployed to production by other teams. It's important to update the Dev Sandboxes to retrieve those changes. It's a multi-step process:

1. Wait till your sprint finishes, and you do not have any changes on the Dev environment which are not yet in the SIT branch.

1. If you do not have any pending changes on the SIT/UAT environment then refresh your Dev Sandbox from Production otherwise refresh it from SIT/UAT depending upon your need and scenario.