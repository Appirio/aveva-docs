## Manual Steps Sheets

Ask your Release manager to provide your team the appropriate Manual Sheets related to your project, if your story needs any manual steps to be performed make sure to keep a track of them in this sheet. Make sure the steps are easy enough that a layman can perform them on the org. Here's a sample screenshot of a manual sheets form and entries.

![image alt text](images/manual-steps-form.png)

1. [CPQ Manual Steps Sheet](https://app.smartsheet.com/sheets/H6pPGcJ8pjXr5xr5PmMPFhV9r56f4rqXg7xmv231){:target="_blank"}

1. [BAU Manual Steps Sheet](https://app.smartsheet.com/sheets/C9grQ8j6vx4h536qx73q4cjJwcrPj3vGfGGcpjv1){:target="_blank"}

1. [Service Cloud Manual Steps Sheet](https://app.smartsheet.com/sheets/pJMP9grJ7QHMXjf9R8wWGc8ppgqG2CGjHgQfWwM1){:target="_blank"}

### Attaching relevant files

![image alt text](images/smartsheet-attachment.png)

If the manual step is complex or dependent upon some files then it's a good idea to attach files/screenshots next to your manual step. So that any release manager who is performing that step has everything he needs for making those changes.

## UAT Dev-Hand off Form

Once your story has been tested on SIT and is ready to be moved onto the UAT envrionment. You will fill up the [UAT Dev Hand-off form](https://app.smartsheet.com/b/form/d6968812343f4aaba816ae4014b3f116){:target="_blank"}.

You are required to enter your GitLab Merge Request link and any manual step information related to that story in here. This will send a notification to your team's Release Manager that they need to move a story to UAT enviornment.
