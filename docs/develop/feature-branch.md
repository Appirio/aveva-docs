## Create a feature branch

To start contributing your changes to the version control repository you need to create a branch in the repository.

!!! Note
    For CPQ and BAU, your feature branch should be created from the `SIT` branch in the `Aveva One CRM` repository whereas for Service Cloud this branch should be created from the `QA` branch of `Aveva Service Cloud` repository.

1.  Create a feature branch with the following nomenclature:

        feature/<dev_name>-<story_issue_incident_number>-<story_issue_incident_short_description>

    You could simply run `adx git:branch --name S-12345` if it's a CMC story, if not then just `adx git:branch --name "my change"`. See [adx git:branch](https://dx.appirio.com/docs/vcs-git/naming-conventions/#adx-gitbranch){:target="_blank"} documentation for more information.

1.  Once, you're on your feature branch, you could start your development using VSCode and on Salesforce. (See _[making changes](https://appirio.gitlab.io/aveva-docs/develop/making-changes/)_ for details).

1.  When you have finished your changes and are ready to move your work to QA/SIT/finish for the day commit your changes in the feature branch after retrieving from the org in VSCode/local branch (See _[retrieving changes](https://appirio.gitlab.io/aveva-docs/develop/retrieving-changes/)_ for details).

	!!!Info
		Only commit your changes, do not blindly replace the file in version control system. A metadata file might have changes from other teams' which you might be overriding. So always make sure to just commit your changes.

1.  Once you `commit and push your changes` to your feature branch it will trigger a validation against the actual QA/SIT org. You can see the success and failure of that on GitLab by navigating to `Gitlab > Aveva One CRM/Aveva Service Cloud > CI/CD > Pipelines/Jobs`.

    ![image alt text](images/pipelines.png)

1. Your validation should succeed and show a green check like in the above image, or else if you have any validation errors it'll show up on the job page like this:

    ![image alt text](images/failed-job.png)

1. You need to resolve these errors by adding any relevant metadata you missed adding inn your previous commits, or by performing any manual steps on the QA/SIT environment yourself.

## Merge Requests

### Updating your branch with latest code from target

You created your branch(let's say feature/sdeep-S-523566) from the target branch(let's say master) a day back. You made some changes in your branch and now you want to merge your changes or send your changes back to the target(master) branch so they can move to the upper environments.

Meanwhile, someone merged some changes in the target(master) branch which are not present in your branch. These changes may be to the same metadata on which you're working or maybe on a different metadata but the point is they're already deployed to the target environment now whereas your branch doesn't have access to those changes.

![image alt text](images/merging-master-into-features.png)

Now, when you create a merge request it is possible that you start seeing merge conflicts, so to ensure we do not face the conflicts, we need to merge the target branch in our branch before raising our merge request to the target branch. That forces each developer (who knows their code changes best) to resolve conflicts with the master branch (if any) rather than requiring the reviewer to make judgements about how merge conflicts should be handled.

### Create a Merge Request

1. When the validation job in your branch has succeeded you can go ahead and create a merge request from your source branch(feature branch) to the traget branch(QA/SIT) and assign it to your Tech-Lead to review and merge.

1. Tech lead gets the email-notification and opens the merge-request, verify changes and clicks on merge to merge those changes to the target (QA/SIT) branch.

### Additional options

![image alt text](images/merge-req-opts.png)

#### Squashing Commits

!!! tip
    Squashing commits is **not a required** step, it's purely on your own judgement. If you feel you have plenty of inntermediate commits in your feature branch which do not contribute to the main commit history then Squash your commits, otherwise don't.

When working on a feature branch, you sometimes want to commit your current progress, but don’t really care about the commit messages. Those ‘work in progress commits’ don’t necessarily contain important information and as such you’d rather not include them in your target branch. Squashing as the name suggests squashes multiple commits into a single meaningful commit. This way, the history of your base branch remains clean with meaningful commit messages. Please [check out the GitLab documentation on how to squash commits automatically with a merge request.](https://docs.gitlab.com/ee/user/project/merge_requests/squash_and_merge.html){:target="_blank"}

#### Removing Source Branch

To keep the repository clean and manageable, it is a good practice to check this option so that when your merge request gets approved your branch gets deleted. If we do not delete branches, we can quickly have tens of branches in the repo and they'd be difficult to manage.


