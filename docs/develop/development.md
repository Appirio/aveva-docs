# Getting Started with Development

There are a few steps involved before beginning actual development like:

## Cloning the repository

[Follow these steps to clone Aveva repository on your local machine](https://dx.appirio.com/docs/project-setup/joining-existing-project/{:target="_blank"}). Once completed, you'd see the project listed on the Appirio DX desktop home page like this:

![image alt text](images/projects.png)

## Connect your project with your Dev Org

1. To connect your Development environment with Appirio DX, you need to open the project:
   ![image alt text](images/open-project.png)

1. Then click on the bottom left link called `Add Production Org or Sandbox` which will then open the following dialogue:
   ![image alt text](images/add-org.png)

1. If you already have your org authorized and listed here, just select the checkbox against its name or else click on `Connect an Org`
   ![image alt text](images/connect-org.png)

1. Write an alias to recognize the org by and click on `Open & Authorize org`.

1. Your browser window will open up the Salesforce Login screen, use that to login in to your Salesforce org and allow the Global app the permissions if asked.

1. Once successfully logged in come back to the project screen to see your newly authorized org under the project screen on the Desktop App.
