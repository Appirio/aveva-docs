# Prerequisites

You need to install Appirio DX on your machines to get started with DevOps with Aveva.

- [Download Appirio DX](https://dx.appirio.com){:target="_blank"}
- [Appirio DX Documentation](https://dx.appirio.com/docs/){:target="_blank"}
  - [Guide to installing Desktop App](https://dx.appirio.com/docs/tools/desktop-app-installation/){:target="_blank"}
- [Configuring Tools from Desktop App after Installation](https://dx.appirio.com/docs/tools/desktop-app-usage/){:target="_blank"}
- [Testing your setup](https://dx.appirio.com/docs/tools/testing-your-setup/){:target="_blank"}
- [Installing extensions in VSCode](https://dx.appirio.com/docs/tools/additional-setup-salesforce/){:target="_blank"}
- [Appirio DX FAQ](https://dx.appirio.com/docs/training/faq/){:target="_blank"}
