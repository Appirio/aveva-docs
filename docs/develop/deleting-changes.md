When you are deleting something from the org like an Apex class or a field, it's important to not only delete that metadata but its `metafile` as well as `references among other metadata components` as well. So when you delete something from the repo, just go ahead and do a global search in the VSCode to see if their are any components where the deleted metadata component is referred and then proceed to modify that piece of metadata as well.

If not deleted, these references will cause us issues when someone tries to make changes to the metdata who have these references to deleted components.

For example:

![image alt text](images/deleted-classes.png)

What's wrong in the above commit? The author deleted the classes but forgot to delete the metafiles which stayed in the repo:

![image alt text](images/class-metafiles.png)

Also, they forgot to do a global search for any references, so there are still components in the repo which refer the deleted metadata.

![image alt text](images/class-refs.png)

So the next time, if someone tries to make changes to `D365ForecastController_Test.cls` they'll have trouble validating it because it contains references to another class which no longer exists. Hence it is important to make sure we delete/modify all related components together.
