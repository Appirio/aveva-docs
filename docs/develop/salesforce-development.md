# Salesforce Development

Salesforce development is unique from other types of local development
because changes are compiled on the server and not locally.

## Metadata Items

Most Salesforce customizations are represented as metadata items. These
metadata items are files that can be retrieved from the server and
modified locally, then saved back to the server or deployed to another
environment. A detailed list of available metadata items can be found
[\*here\*](https://developer.salesforce.com/docs/atlas.en-us.api_meta.meta/api_meta/meta_types_list.htm).

The items that we do not include are:

- InstalledPackage
- SamlSsoConfig
- NamedCredential
- Certificate
- ConnectedApp
- Dashboards
- Reports
- Documents

## Differential Deployment

During each deployment only the items that have changed since the
last **successful** deployment to the target environment. To read more about this checkou the [differential deployment documentation](https://dx.appirio.com/docs/ci-cd-gitlab/differential-deploy/#differential-deployments-in-org-based-projects-either-legacymdapi-or-source-format).

## force-app/

The **force-app/** directory contains all of the metadata files that have been
retrieved from the server via the **package.xml** file or using **org-browser** or using **SFDX source retrieve commands**. These files are in a folder directory
structure where the Metadata Type Name is the name of the folder and
each metadata file is its own file within that folder. Here is a brief
example of what that may look like.

```
force-app/main/default/
    -classes
        --ClassA..cls
        --ClassA..cls-meta.xml
        --ClassA_Test.cls
        --ClassA_Test.cls-meta.xml
    -pages
        --PageA.page
        --PageA.page-meta.xml
```
## Manual Changes

Not all steps required for a deployment can be represented by Salesforce
metadata. When a manual configuration is needed by the developer they
should document the steps for the Tech Leads and Release Managers and
provide this information as a manifest item (pre/post manual step) in
CMC. The instructions should be as detailed as possible and written as
though someone who is unfamiliar with Salesforce must perform the
associated steps.
