# Making Changes

All changes can be made with either the Salesforce UI or your local IDE.
Most configuration customizations are made with the UI while most code
changes are made within the IDE. To make a change in the IDE you simply
create a new class or page and modify it locally and then save it to the
server. The server will then compile your full class or page and return
the result. If it was successful you update will be saved to the server.
Otherwise you must correct your errors and re-compile on the server.

### Using VSCode and Salesforce Extensions

[A Quick Start: Visual Studio Code for Salesforce Development](https://trailhead.salesforce.com/content/learn/projects/quickstart-vscode-salesforce)

### Getting familiar with the Org-Development Model

The [Org development model](https://trailhead.salesforce.com/content/learn/modules/org-development-model) trailhead is a good starting point on how to get comfortable in using VSCode and the SFDX commands to make changes.

### Org Development Model with VS Code

Also, go through [Org Development Model with VS Code](https://developer.salesforce.com/tools/vscode/articles/user-guide/org-development-model) to use VSCode to deploy code/config changes to your development environment.
