After modifying items on Salesforce UI, you may want to retrieve those
changes and add them to your local working directory/feature branch. In order to do
this you must update your local copy with the server copy. There are a
number of ways to do this using **VSCode Salesforce Extensions**

### sfdx force:source:retrieve

When using this command you again have various options on how to retrieve multiple or single components from the Org. [Checkout its documentation](https://developer.salesforce.com/docs/atlas.en-us.sfdx_cli_reference.meta/sfdx_cli_reference/cli_reference_force_source.htm){:target="_blank"} and an example on how to [Develop Against Any Org.](https://developer.salesforce.com/docs/atlas.en-us.sfdx_dev.meta/sfdx_dev/sfdx_dev_develop_any_org.htm){:target="_blank"}

### Manifest file

This is probably most familiar option. [See the documentation](https://developer.salesforce.com/tools/vscode/articles/user-guide/org-development-model#the-manifest-packagexml-file){:target="_blank"} on how to create one and [how to retrieve using package.xml](https://developer.salesforce.com/tools/vscode/articles/user-guide/org-development-model#retrieve-source){:target="_blank"}

### Org Browser (beta)

After enabling Org-browser, this might seem like the easiest option to use and once it's GA we hope it would decrease the use of pacakge.xml and help admins and developers to easily retrieve metadata. To use it now [checkout its documentation](https://developer.salesforce.com/tools/vscode/articles/user-guide/org-browser){:target="_blank"}
