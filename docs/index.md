# Aveva DevOps Workflow Documentation

## Release Management Highlights

This documentation is meant as a starting point for Developers and teams to get started with using Appirio DX and follow the best practices for development and releasing their features.

## Release Strategy

![image alt text](assets/images/release-strategy.png)

The Release strategy can be bifurcated between `CPQ, BAU` and `Service Cloud`.

* All development teams will have their separate Developer Sandboxes. CPQ having theirs, Service Cloud having theirs and BAU having theirs.

* Development is done on these individual sandboxes and when we think a feature/story is done and ready for Quality Analysis, we put it in the Version Control Repository to be moved for QA.

* `CPQ and BAU` move their changes along `SIT > UAT > Production` path.

* `Service Cloud` moves their changes along `QA > SIT > UAT > Production` path.

    !!! Note
        This is done because there are no overlapping, close releases between CPQ and Service Cloud the two big projects. CPQ is in full swing and will probably be close to ending or would be ended by the time Service Cloud wants to move to higher environments. Thus to keep SIT clean and up to date with changes that are surely moving to UAT in near future this is being done.

* `SIT > UAT > Production` are linked with `Aveva One CRM` repository. This is the master repository which all projects will use to move changes to these environments. SIT environnment corresponds to an SIT branch in this repository, UAT env. is linked to a UAT branch and Production environment is tied to the master branch of this repository.

* `Service Cloud` has their own repository `Aveva Service Cloud` for moving changes to their QA and Data Migration Sandboxes. This repository will be forked from `Aveva One CRM` repository so these two repositories can communicate with each other. The QA branch of this repository is tied to the `QA` environment of the Service Cloud team.

* From SIT, selective changes are moved to UAT where the Client does User Acceptance Testing and when we have a go ahead, everything on UAT is moved to Production environment.
